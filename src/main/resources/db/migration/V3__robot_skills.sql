CREATE TABLE robot_skills (
  robot_id BIGINT REFERENCES robot (id),
  task_id BIGINT REFERENCES task (id),
  CONSTRAINT robot_skills_pkey PRIMARY KEY (robot_id, task_id)
);
