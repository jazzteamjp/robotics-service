CREATE TABLE robot_skills_default (
  robot_type TEXT CHECK (robot_type IN ('BENDER', 'WALLE')),
  task_id BIGINT REFERENCES task (id),
  CONSTRAINT robot_skills_default_pkey PRIMARY KEY (robot_type, task_id)
);
