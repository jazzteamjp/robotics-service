CREATE TABLE task (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  duration INT NOT NULL,
  is_killing BOOLEAN DEFAULT false NOT NULL
);

CREATE TABLE robot (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  created_on timestamp NOT NULL,
  died_on TIMESTAMP
);

CREATE TABLE robot_bender (
  id BIGINT PRIMARY KEY REFERENCES robot (id),
  kiss_my_ass_message VARCHAR(150) NOT NULL,
  beers_per_second FLOAT(25) NOT NULL,
  dollars_robed_per_minute FLOAT(25) NOT NULL
);

CREATE TABLE job (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  task_id BIGINT REFERENCES task (id) NOT NULL,
  robot_id BIGINT REFERENCES robot (id) NOT NULL,
  started_on timestamp  NOT NULL,
  ended_on timestamp
);
