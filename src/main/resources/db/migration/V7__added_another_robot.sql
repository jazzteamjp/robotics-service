CREATE TABLE robot_walle (
  id BIGINT PRIMARY KEY REFERENCES robot (id),
  tons_of_garbage_per_day FLOAT(25) NOT NULL
);
