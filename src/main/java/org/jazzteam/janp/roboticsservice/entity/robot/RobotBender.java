package org.jazzteam.janp.roboticsservice.entity.robot;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity(name = "robot_bender")
@Data
public class RobotBender extends Robot {

    @NotNull
    @Column(name = "kiss_my_ass_message")
    protected String kissMyAssMessage;

    @NotNull
    @Column(name = "beers_per_second")
    protected double beersPerSecond;

    @NotNull
    @Column(name = "dollars_robed_per_minute")
    protected double dollarsRobedPerMinute;

    public RobotBender() {
        super();

    }

    @Override
    public String speak() {
        return String.format("Drunk %f beers and robed %f dollars! %s", beersPerSecond, dollarsRobedPerMinute, kissMyAssMessage);
    }
}
