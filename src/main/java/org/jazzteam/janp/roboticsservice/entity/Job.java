package org.jazzteam.janp.roboticsservice.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jazzteam.janp.roboticsservice.entity.robot.Robot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "started_on")
    private LocalDateTime startedOn;

    @Column(name = "ended_on")
    private LocalDateTime endedOn;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id", nullable = false)
    private Task task;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "robot_id", nullable = false)
    private Robot robot;
}
