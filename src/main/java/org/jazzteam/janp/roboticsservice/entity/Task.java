package org.jazzteam.janp.roboticsservice.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jazzteam.janp.roboticsservice.entity.robot.Robot;
import org.jazzteam.janp.roboticsservice.enums.RobotType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int duration;

    @Column(name = "is_killing")
    private boolean isKilling;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "robot_skills",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "robot_id")
    )
    protected List<Robot> robots = new ArrayList<>();

    @ElementCollection
    @CollectionTable(
            name = "robot_skills_default",
            joinColumns = @JoinColumn(name = "task_id")
    )
    @Column(name = "robot_type")
    @Enumerated(EnumType.STRING)
    protected Set<RobotType> defaultRobots = new HashSet<>();
}
