package org.jazzteam.janp.roboticsservice.entity.robot;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
public abstract class Robot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @NotNull
    protected String name;

    @Column(name = "created_on", updatable = false)
    protected LocalDateTime createdOn;

    @Column(name = "died_on")
    protected LocalDateTime diedOn;

    @Column(name = "last_task_end")
    protected LocalDateTime lastTaskEnd;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Robot robot = (Robot) o;
        return Objects.equals(id, robot.id) &&
                Objects.equals(name, robot.name) &&
                Objects.equals(createdOn, robot.createdOn) &&
                Objects.equals(diedOn, robot.diedOn) &&
                Objects.equals(lastTaskEnd, robot.lastTaskEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, createdOn, diedOn, lastTaskEnd);
    }

    public abstract String speak();
}
