package org.jazzteam.janp.roboticsservice.entity.robot;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity(name = "robot_walle")
@Data
public class RobotWalle extends Robot {

    @NotNull
    @Column(name = "tons_of_garbage_per_day")
    protected double tonsOfGarbagePerDay;

    public RobotWalle() {
        super();
    }

    @Override
    public String speak() {
        return String.format("Collected %f tons of garbage!", tonsOfGarbagePerDay);
    }
}
