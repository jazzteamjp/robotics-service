package org.jazzteam.janp.roboticsservice.utils;

import java.time.format.DateTimeFormatter;

public class Constants {

    public static final DateTimeFormatter spacedDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

}
