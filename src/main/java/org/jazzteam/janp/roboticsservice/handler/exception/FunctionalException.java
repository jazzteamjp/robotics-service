package org.jazzteam.janp.roboticsservice.handler.exception;

public class FunctionalException extends RuntimeException {

    public FunctionalException(String message) {
        super(message);
    }

}
