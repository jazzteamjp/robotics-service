package org.jazzteam.janp.roboticsservice.handler;

import org.jazzteam.janp.roboticsservice.handler.exception.BadRequestException;
import org.jazzteam.janp.roboticsservice.handler.exception.FunctionalException;
import org.jazzteam.janp.roboticsservice.handler.exception.NotFoundException;
import org.jazzteam.janp.roboticsservice.handler.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.Instant;

@RestControllerAdvice
public class ExceptionHandlers {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlers.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public ErrorInfo handleBadRequestException(BadRequestException e) {
        return getErrorInfo(e, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(FunctionalException.class)
    public ErrorInfo handleFunctionalException(FunctionalException e) {
        return getErrorInfo(e, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    public ErrorInfo handleValidationException(ValidationException e) {
        return getErrorInfo(e, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ErrorInfo handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return buildErrorInfo(e, "Invalid input type.", HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorInfo handleNotFoundException(NotFoundException e) {
        return getErrorInfo(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorInfo handleException(MethodArgumentNotValidException exception) {

        String errorMsg = exception.getBindingResult().getAllErrors().stream()
                .findFirst()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .orElse(exception.getMessage());

        return buildErrorInfo(exception, errorMsg, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorInfo handleException(ConstraintViolationException exception) {

        String errorMsg = exception.getConstraintViolations().stream()
                .findFirst()
                .map(ConstraintViolation::getMessage)
                .orElse(exception.getMessage());

        return buildErrorInfo(exception, errorMsg, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorInfo handleException(Exception e) {
        LOG.error(e.getMessage(), e);
        return buildDefaultErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ErrorInfo buildErrorInfo(Exception e, String message, HttpStatus status) {
        return new ErrorInfo(status.value(), status.getReasonPhrase(), e.toString(), message, Instant.now().getEpochSecond());
    }

    private ErrorInfo getErrorInfo(Exception e, HttpStatus status) {
        return buildErrorInfo(e, e.getMessage(), status);
    }

    private ErrorInfo buildDefaultErrorInfo(HttpStatus status) {
        return new ErrorInfo(status.value(), status.getReasonPhrase(), "",
                "Something went wrong, please contact your system administrator. The action was not completed.",
                Instant.now().getEpochSecond());
    }

}
