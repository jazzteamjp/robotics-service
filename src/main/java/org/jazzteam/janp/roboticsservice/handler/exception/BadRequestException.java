package org.jazzteam.janp.roboticsservice.handler.exception;

public class BadRequestException extends RuntimeException {

    public BadRequestException(String message) {
        super(message);
    }

}
