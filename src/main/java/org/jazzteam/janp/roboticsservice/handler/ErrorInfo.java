package org.jazzteam.janp.roboticsservice.handler;

@SuppressWarnings("WeakerAccess")
class ErrorInfo {

    public final int status;
    public final String error;
    public final String message;
    public final String exception;
    public final long timestamp;

    ErrorInfo(int status, String error, String exception, String message, long timestamp) {
        this.status = status;
        this.message = message;
        this.exception = exception;
        this.error = error;
        this.timestamp = timestamp;
    }

}
