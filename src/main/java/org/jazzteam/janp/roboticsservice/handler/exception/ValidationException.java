package org.jazzteam.janp.roboticsservice.handler.exception;

public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }

}