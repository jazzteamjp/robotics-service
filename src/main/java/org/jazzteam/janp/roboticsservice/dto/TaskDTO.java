package org.jazzteam.janp.roboticsservice.dto;

import org.jazzteam.janp.roboticsservice.entity.Task;
import org.jazzteam.janp.roboticsservice.enums.RobotType;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

/**
 * @author Jan Palaznik
 * @since 10-Dec-17.
 */
public class TaskDTO {
    public final Long id;
    public final String name;
    public final int duration;
    public final boolean isKilling;
    public final List<RobotDTO> robots;
    public final Set<RobotType> robotTypes;

    public TaskDTO(Task task) {
        this.id = task.getId();
        this.duration = task.getDuration();
        this.isKilling = task.isKilling();
        this.name = task.getName();
        this.robots = task.getRobots().stream().map(RobotDTO::new).collect(toList());
        this.robotTypes = task.getDefaultRobots();
    }
}
