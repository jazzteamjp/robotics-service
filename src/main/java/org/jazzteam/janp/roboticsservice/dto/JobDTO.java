package org.jazzteam.janp.roboticsservice.dto;

import org.jazzteam.janp.roboticsservice.entity.Job;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.jazzteam.janp.roboticsservice.utils.Constants.spacedDateTimeFormatter;

public class JobDTO {

    public final Long id;
    public final String startedOn;
    public final String endedOn;
    public final String taskName;
    public final String robotName;
    public final String message;

    public JobDTO(Job job) {
        this.id = job.getId();
        this.startedOn = job.getStartedOn().format(spacedDateTimeFormatter);
        this.endedOn = job.getEndedOn().format(spacedDateTimeFormatter);
        this.taskName = job.getTask().getName();
        this.robotName = job.getRobot().getName();
        this.message = job.getRobot().speak();
    }
}
