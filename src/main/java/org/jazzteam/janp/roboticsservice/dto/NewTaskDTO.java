package org.jazzteam.janp.roboticsservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NewTaskDTO {
    public final String name;
    public final int duration;
    public final boolean isKilling;

    @JsonCreator
    public NewTaskDTO(@JsonProperty("name") String name,
                      @JsonProperty("duration") int duration,
                      @JsonProperty("isKilling") boolean isKilling) {
        this.name = name;
        this.duration = duration;
        this.isKilling = isKilling;
    }
}
