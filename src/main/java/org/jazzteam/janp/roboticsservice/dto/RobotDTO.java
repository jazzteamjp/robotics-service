package org.jazzteam.janp.roboticsservice.dto;

import org.jazzteam.janp.roboticsservice.entity.robot.Robot;

import java.time.LocalDateTime;

import static org.jazzteam.janp.roboticsservice.utils.Constants.spacedDateTimeFormatter;

/**
 * @author Jan Palaznik
 * @since 10-Dec-17.
 */
public class RobotDTO {

    public final Long id;
    public final String name;
    public final String createdOn;
    public final boolean isDied;
    public final String lastTaskEnd;

    public RobotDTO(Robot robot) {
        this.id = robot.getId();
        this.name = robot.getName();
        this.createdOn = robot.getCreatedOn().format(spacedDateTimeFormatter);
        this.isDied = robot.getDiedOn() != null;
        this.lastTaskEnd = robot.getLastTaskEnd().format(spacedDateTimeFormatter);
    }
}
