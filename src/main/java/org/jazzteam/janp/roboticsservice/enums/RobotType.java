package org.jazzteam.janp.roboticsservice.enums;

import org.jazzteam.janp.roboticsservice.entity.robot.Robot;
import org.jazzteam.janp.roboticsservice.entity.robot.RobotBender;
import org.jazzteam.janp.roboticsservice.entity.robot.RobotWalle;

import java.time.LocalDateTime;
import java.util.Random;

/**
 * @author Jan Palaznik
 * @since 08-Dec-17.
 */
public enum RobotType {

    BENDER {
        @Override
        public Robot createRobot() {
            RobotBender robotBender = new RobotBender();
            LocalDateTime now = LocalDateTime.now();
            robotBender.setCreatedOn(now);
            robotBender.setLastTaskEnd(now);
            robotBender.setName("Bender " + random.nextInt(10000));
            robotBender.setKissMyAssMessage("Kill all humans. You meatbags had your chance.");
            robotBender.setBeersPerSecond(random.nextDouble() * 10);
            robotBender.setDollarsRobedPerMinute(random.nextDouble() * 10);
            return robotBender;
        }
    },
    WALLE {
        @Override
        public Robot createRobot() {
            RobotWalle robotBender = new RobotWalle();
            LocalDateTime now = LocalDateTime.now();
            robotBender.setCreatedOn(now);
            robotBender.setLastTaskEnd(now);
            robotBender.setName("Wall-e " + random.nextInt(10000));
            robotBender.setTonsOfGarbagePerDay(random.nextDouble() * 100);
            return robotBender;
        }
    };

    private static Random random = new Random();

    public abstract Robot createRobot();
}
