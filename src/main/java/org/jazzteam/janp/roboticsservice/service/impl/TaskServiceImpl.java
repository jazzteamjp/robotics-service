package org.jazzteam.janp.roboticsservice.service.impl;

import org.jazzteam.janp.roboticsservice.dto.JobDTO;
import org.jazzteam.janp.roboticsservice.dto.NewTaskDTO;
import org.jazzteam.janp.roboticsservice.dto.TaskDTO;
import org.jazzteam.janp.roboticsservice.entity.Job;
import org.jazzteam.janp.roboticsservice.entity.Task;
import org.jazzteam.janp.roboticsservice.entity.robot.Robot;
import org.jazzteam.janp.roboticsservice.enums.RobotType;
import org.jazzteam.janp.roboticsservice.handler.exception.BadRequestException;
import org.jazzteam.janp.roboticsservice.repository.JobRepository;
import org.jazzteam.janp.roboticsservice.repository.RobotRepository;
import org.jazzteam.janp.roboticsservice.repository.TaskRepository;
import org.jazzteam.janp.roboticsservice.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final RobotRepository robotRepository;
    private final JobRepository jobRepository;
    private final Random random = new Random();
    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, RobotRepository robotRepository, JobRepository jobRepository) {
        this.taskRepository = taskRepository;
        this.robotRepository = robotRepository;
        this.jobRepository = jobRepository;
    }

    @Override
    public void createJobs(Long taskId, Boolean broadcast) {
        Task task = findTaskById(taskId);
        Stream<Robot> robotStream = extractRobotsForJob(broadcast, task);
        robotStream.forEach(robot -> assignJob(task, robot));
    }

    private Stream<Robot> extractRobotsForJob(Boolean broadcast, Task task) {
        LocalDateTime readyTime = LocalDateTime.now().plusSeconds(5L);
        return broadcast ? extractRobotsForBroadcastJob(task, readyTime)
                : extractOneRobotForJob(task, readyTime);
    }

    private Stream<Robot> extractRobotsForBroadcastJob(Task task, LocalDateTime readyTime) {
        Stream<Robot> robotStream;
        if (task.isKilling()) {
            robotStream = task.getRobots().stream().filter(robot -> robot.getDiedOn() != null);
        } else {
            int amountNotReady = robotRepository.countNotReadyForTask(readyTime, task.getId());
            List<Robot> readyRobots = robotRepository.findAllReadyForTask(readyTime, task.getId());
            robotStream = Stream.concat(readyRobots.stream(), createHelpRobots(task, amountNotReady));
        }
        return robotStream;
    }

    private Stream<Robot> extractOneRobotForJob(Task task, LocalDateTime readyTime) {
        Robot readyRobot = robotRepository.findFirstReadyForTask(readyTime, task.getId())
                .stream()
                .findAny()
                .orElse(null);
        if (readyRobot == null) {
            return createHelpRobots(task, 1);
        }
        return Stream.of(readyRobot);
    }

    private Stream<Robot> createHelpRobots(Task task, int amount) {
        if (amount == 0) {
            return Stream.empty();
        }
        Set<RobotType> defaultRobots = task.getDefaultRobots();
        RobotType[] robotTypes = defaultRobots.toArray(new RobotType[]{});
        List<Robot> robotStream = IntStream.range(0, amount)
                .mapToObj(i -> createRandomDefaultRobot(task, defaultRobots, robotTypes)).collect(toList());
        taskRepository.save(task);
        return robotStream.stream();
    }

    private Robot createRandomDefaultRobot(Task task, Set<RobotType> defaultRobots, RobotType[] robotTypes) {
        int index = random.nextInt(defaultRobots.size());
        RobotType type = robotTypes[index];
        Robot robot = type.createRobot();
        robotRepository.save(robot);
        task.getRobots().add(robot);
        return robot;
    }

    private Task findTaskById(Long taskId) {
        Task task = taskRepository.findOne(taskId);
        if (task == null) {
            throw new BadRequestException("Task wasn't found");
        }
        return task;
    }

    private void assignJob(Task task, Robot readyRobot) {
        LocalDateTime jobStart = LocalDateTime.now();
        if (jobStart.isBefore(readyRobot.getLastTaskEnd())) {
            jobStart = readyRobot.getLastTaskEnd().plusSeconds(1L);
        }
        LocalDateTime jobEnd = jobStart.plusSeconds(task.getDuration());
        updateRobotEndTimes(readyRobot, jobEnd, task.isKilling());
        createJob(task, readyRobot, jobStart, jobEnd);
    }

    private void updateRobotEndTimes(Robot readyRobot, LocalDateTime jobEnd, boolean isKilling) {
        readyRobot.setLastTaskEnd(jobEnd);
        if (isKilling) {
            readyRobot.setDiedOn(jobEnd);
        }
        robotRepository.save(readyRobot);
    }

    private void createJob(Task task, Robot readyRobot, LocalDateTime jobStart, LocalDateTime jobEnd) {
        Job job = new Job();
        job.setRobot(readyRobot);
        job.setEndedOn(jobEnd);
        job.setStartedOn(jobStart);
        job.setTask(task);
        jobRepository.save(job);
    }

    @Override
    public List<TaskDTO> getAllTasks() {
        return taskRepository.findAll()
                .stream()
                .map(TaskDTO::new)
                .collect(toList());
    }

    @Override
    public List<JobDTO> getLogs(Long lastJobId) {
        List<Job> jobs;
        if (lastJobId == null) {
            jobs = jobRepository.findAllSorted();
        } else {
            jobs = jobRepository.findAllAfterId(lastJobId);
        }
        return jobs.stream()
                .map(JobDTO::new)
                .collect(toList());
    }

    @Override
    public void addBasicSkill(Long taskId, RobotType robotType) {
        Task task = findTaskById(taskId);
        task.getDefaultRobots().add(robotType);
        taskRepository.save(task);
    }

    @Override
    public void createTask(NewTaskDTO newTask) {
        Task task = new Task();
        task.setName(newTask.name);
        task.setDuration(newTask.duration);
        task.setKilling(newTask.isKilling);
        taskRepository.save(task);
    }
}
