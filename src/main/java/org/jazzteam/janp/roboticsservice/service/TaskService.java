package org.jazzteam.janp.roboticsservice.service;

import org.jazzteam.janp.roboticsservice.dto.JobDTO;
import org.jazzteam.janp.roboticsservice.dto.NewTaskDTO;
import org.jazzteam.janp.roboticsservice.dto.TaskDTO;
import org.jazzteam.janp.roboticsservice.enums.RobotType;

import java.util.List;

public interface TaskService {

    void createJobs(Long taskId, Boolean broadcast);

    List<TaskDTO> getAllTasks();

    List<JobDTO> getLogs(Long lastJobId);

    void addBasicSkill(Long taskId, RobotType robotType);

    void createTask(NewTaskDTO newTask);
}
