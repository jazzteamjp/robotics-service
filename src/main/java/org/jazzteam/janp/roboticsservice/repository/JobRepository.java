package org.jazzteam.janp.roboticsservice.repository;

import org.jazzteam.janp.roboticsservice.entity.Job;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
    @EntityGraph(attributePaths = {"robot", "task"})
    @Query("SELECT job FROM Job job WHERE job.id > ?1 ORDER BY job.startedOn DESC")
    List<Job> findAllAfterId(Long lastJobId);

    @EntityGraph(attributePaths = {"robot", "task"})
    @Query("SELECT job FROM Job job ORDER BY job.startedOn DESC")
    List<Job> findAllSorted();
}
