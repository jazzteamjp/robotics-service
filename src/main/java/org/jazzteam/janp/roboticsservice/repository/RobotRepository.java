package org.jazzteam.janp.roboticsservice.repository;

import org.jazzteam.janp.roboticsservice.entity.robot.Robot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RobotRepository extends JpaRepository<Robot, Long> {

    @Query("SELECT robot FROM Task task JOIN task.robots robot " +
            "WHERE robot.lastTaskEnd < ?1 AND robot.diedOn is null AND task.id = ?2")
    List<Robot> findFirstReadyForTask(LocalDateTime readyTime, Long taskId);

    @Query("SELECT robot FROM Task task JOIN task.robots robot " +
            "WHERE robot.lastTaskEnd < ?1 AND robot.diedOn is null AND task.id = ?2")
    List<Robot> findAllReadyForTask(LocalDateTime readyTime, Long taskId);

    @Query("SELECT count(robot) FROM Task task JOIN task.robots robot " +
            "WHERE robot.lastTaskEnd >= ?1 AND robot.diedOn is null AND task.id = ?2")
    int countNotReadyForTask(LocalDateTime readyTime, Long taskId);

}
