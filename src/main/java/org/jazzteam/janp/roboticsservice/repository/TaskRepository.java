package org.jazzteam.janp.roboticsservice.repository;

import org.jazzteam.janp.roboticsservice.entity.Task;
import org.jazzteam.janp.roboticsservice.entity.robot.Robot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
