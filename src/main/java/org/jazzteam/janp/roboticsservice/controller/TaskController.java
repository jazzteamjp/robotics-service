package org.jazzteam.janp.roboticsservice.controller;

import org.jazzteam.janp.roboticsservice.dto.JobDTO;
import org.jazzteam.janp.roboticsservice.dto.NewTaskDTO;
import org.jazzteam.janp.roboticsservice.dto.TaskDTO;
import org.jazzteam.janp.roboticsservice.entity.Job;
import org.jazzteam.janp.roboticsservice.enums.RobotType;
import org.jazzteam.janp.roboticsservice.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/{taskId}")
    public void sendTask(@RequestParam Boolean broadcast,
                         @PathVariable Long taskId) {
        taskService.createJobs(taskId, broadcast);
    }

    @GetMapping
    public List<TaskDTO> getAllTasks() {
        return taskService.getAllTasks();
    }

    @GetMapping("/logs")
    public List<JobDTO> getLogs(@RequestParam(required = false) Long lastJobId) {
        return taskService.getLogs(lastJobId);
    }

    @GetMapping("/types")
    public RobotType[] getRobotTypes() {
        return RobotType.values();
    }

    @PostMapping("/{taskId}/types/{robotType}")
    public void addBasicSkill(@PathVariable Long taskId, @PathVariable RobotType robotType) {
        taskService.addBasicSkill(taskId, robotType);
    }

    @PostMapping
    public void createNewTask(@RequestBody NewTaskDTO newTask) {
        taskService.createTask(newTask);
    }

}
